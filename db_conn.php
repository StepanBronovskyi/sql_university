<?php

class DB {
    const HOST = 'localhost';
    const USER = 'root';
    const PASSWORD = '';
    const DB_NAME = 'university_db';

    public $db;

    public function __construct() {
        $this->db = new mysqli(HOST, USER, PASSWORD, DB_NAME);
    }

    public function getAllFromEntity($table, $limit) {
        if (mysqli_connect_errno()) {
            printf("Подключение к серверу MySQL невозможно. Код ошибки: %s\n", mysqli_connect_error());
            exit;
        }
        $query = "SELECT * FROM $table LIMIT $limit";
        if ($result = $this->db->query($query)) {
            return $result->num_rows;
        }
    }
}

$d = new DB();
print_r($d->getAllFromEntity('category', 10));
