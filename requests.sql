SELECT first_name, last_name, middle_name FROM employee;

SELECT name FROM product;

SELECT street, number FROM address;

SELECT name, salary FROM possition;

SELECT first_name, last_name FROM employee
WHERE experience > 20

SELECT category.name, product.recept, product.price, COUNT(1) AS Count_by_category
  FROM product, shopping, category
  WHERE product.id = shopping.product_id AND category.id = product.category_id
GROUP BY category.name, product.price, product.recept

SELECT product.name, product.price, shopping.count
  FROM product, shopping
  WHERE product.id = shopping.product_id
GROUP BY product.name, product.price, shopping.count

SELECT category.name, COUNT(1) as Count_of_product
  FROM category, product
  WHERE category.id = product.category_id
GROUP BY category.name

SELECT e.first_name, e.last_name, p.name, a.city, a.street, a.number
FROM employee e, address a, possition p
WHERE e.address_id = a.id AND e.possition_id = p.id
ORDER BY e.last_name DESC

SELECT pharmacy.id, address.city, address.street, address.number, COUNT(1) as Count_by_pharm
  FROM pharmacy, shopping, address
  WHERE pharmacy.id = shopping.pharmacy_id AND pharmacy.address_id = address.id
GROUP BY pharmacy.id, address.city, address.street, address.number


SELECT employee.first_name, employee.first_name, employee.middle_name, possition.name, employee.experience
  FROM employee, possition
  WHERE employee.possition_id = possition.id AND
  employee.pharmacy_id IN (SELECT pharmacy.id FROM pharmacy WHERE pharmacy.work_time = 'Цілодобово')

SELECT p.name, c.name, p.active, p.recept, p.price
  FROM product p, category c
  WHERE p.category_id = c.id AND p.price > (SELECT AVG(product.price) FROM product)
ORDER BY p.price DESC

SELECT employee.last_name, employee.first_name, employee.middle_name, possition.name, employee.age
  FROM employee, possition
  WHERE employee.possition_id = possition.id AND
  employee.address_id IN (SELECT address.id FROM address WHERE address.city = 'Чернівці')

SELECT product.name, category.name, product.recept, product.experience_date, product.price, product.count
  FROM product, category
  WHERE product.category_id = category.id AND
  product.id NOT IN (SELECT shopping.product_id FROM shopping)

SELECT * FROM product
  WHERE product.id =
    (SELECT shopping.product_id FROM shopping WHERE shopping.count = (SELECT MAX(shopping.count) FROM shopping))


