CREATE VIEW Employee_view(first_name, last_name, middle_name) AS SELECT first_name, last_name, middle_name FROM employee;

CREATE VIEW Product_names_view(names) AS SELECT name FROM product;

CREATE VIEW Pharmacy_address_view(street, number) AS SELECT street, number FROM address;

CREATE VIEW Salary_view(name, salary) AS SELECT name, salary FROM possition;

CREATE VIEW Old_mployee_view(name, last_name) AS SELECT first_name, last_name FROM employee
WHERE experience > 20

CREATE VIEW Category_view(name, recept, price) AS SELECT category.name, product.recept, product.price, COUNT(1) AS Count_by_category
  FROM product, shopping, category
  WHERE product.id = shopping.product_id AND category.id = product.category_id
GROUP BY category.name, product.price, product.recept

CREATE VIEW Products_view(name, price, shopping) AS SELECT product.name, product.price, shopping.count
  FROM product, shopping
  WHERE product.id = shopping.product_id
GROUP BY product.name, product.price, shopping.count

CREATE VIEW Category_product_count_view(name) AS SELECT category.name, COUNT(1) as Count_of_product
  FROM category, product
  WHERE category.id = product.category_id
GROUP BY category.name

CREATE VIEW Employee_info_view(name, last_name, possition, city, street, home_address) AS SELECT e.first_name, e.last_name, p.name, a.city, a.street, a.number
FROM employee e, address a, possition p
WHERE e.address_id = a.id AND e.possition_id = p.id
ORDER BY e.last_name DESC

CREATE VIEW Pharmacy_info_view(pharmacy, city, street, address, count) AS SELECT pharmacy.id, address.city, address.street, address.number, COUNT(1) as Count_by_pharm
  FROM pharmacy, shopping, address
  WHERE pharmacy.id = shopping.pharmacy_id AND pharmacy.address_id = address.id
GROUP BY pharmacy.id, address.city, address.street, address.number


CREATE VIEW Employee_possition_view(name, l_name, m_name, possition, experience) AS SELECT employee.first_name, employee.last_name, employee.middle_name, possition.name, employee.experience
  FROM employee, possition
  WHERE employee.possition_id = possition.id AND
  employee.pharmacy_id IN (SELECT pharmacy.id FROM pharmacy WHERE pharmacy.work_time = 'Цілодобово')

CREATE VIEW Product_info_view(name, category, activity, recept, price) AS SELECT p.name, c.name, p.active, p.recept, p.price
  FROM product p, category c
  WHERE p.category_id = c.id AND p.price > (SELECT AVG(product.price) FROM product)
ORDER BY p.price DESC

CREATE VIEW Employee_info_detail_view(l_name, f_name, m_name, possition, age) AS SELECT employee.last_name, employee.first_name, employee.middle_name, possition.name, employee.age
  FROM employee, possition
  WHERE employee.possition_id = possition.id AND
  employee.address_id IN (SELECT address.id FROM address WHERE address.city = 'Чернівці')

CREATE VIEW Product_detail_view(name, category, recept, exp_date, price, count) AS SELECT product.name, category.name, product.recept, product.experience_date, product.price, product.count
  FROM product, category
  WHERE product.category_id = category.id AND
  product.id NOT IN (SELECT shopping.product_id FROM shopping)

CREATE VIEW Recomended_product_view AS SELECT * FROM product
  WHERE product.id =
    (SELECT shopping.product_id FROM shopping WHERE shopping.count = (SELECT MAX(shopping.count) FROM shopping))


